# Background Task Bundle

[![Packagist](https://img.shields.io/packagist/v/text-media/background-task-bundle.svg)](https://packagist.org/packages/text-media/background-task-bundle)
[![Packagist](https://img.shields.io/packagist/l/text-media/background-task-bundle.svg)](https://packagist.org/packages/text-media/background-task-bundle)

Модуль Symfony для фоновых задач

## Установка

Модуль устанавливается через добавление зависимости в composer

```bash
composer require text-media/background-task-bundle
```

В конфиге doctrine необходимо описать Entry Manager с именем `background_task` и отдельным подключением

Активировать модуль в `Kernel::registerBundles`

Выполнить миграцию автоматически или вручную

```bash
php ./bin/console doctrine:schema:update --em=background_task --dump-sql
```

## Использование

В контейнере должен быть зарегистрирован хотя бы один сервис реализующий интерфейс `ProviderInterface`
 
```php
<?php

namespace MyBundle;

use TextMedia\BackgroundTaskBundle\ProviderInterface;

class MyService implements ProviderInterface
{
    /**
     * Получить список обработчиков по именам задач.
     *
     * @return callable[]
     */
    public function getBackgroundCallbacks(): array
    {
        return [
            'task:trim' => 'trim',
            'task:pow' => 'pow'
        ];
    }
}

```

```yaml
services:
    my_service:
        class: MyBundle\MyService
        tags:
            - { name: "background_task.provider", alias: "my_service"}
```

Создать фоновую задачу можно с помощью метода TaskManagerInterface::addTask
```php
/** @var \TextMedia\BackgroundTaskBundle\TaskManagerInterface $taskManager */
$taskManager = $container->get('background_task.manager');
$taskManager->addTask(1, 'task:pow', [2,3]);
```

Выполнение фоновых задач осуществляется с помощью команды
```bash
php ./bin/console background:task:run
```

Онформация о параметрах команды
```bash
php ./bin/console background:task:run -h
```

Если коллбек выбрасывает исключение `DeferredException` задача будет отложена
