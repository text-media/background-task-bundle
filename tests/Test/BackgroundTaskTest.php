<?php

namespace TextMedia\BackgroundTaskBundle\Tests\Test;

use TextMedia\BackgroundTaskBundle\Entity\Task;
use TextMedia\BackgroundTaskBundle\TaskManagerInterface;
use TextMedia\BackgroundTaskBundle\TaskRunnerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BackgroundTaskTest extends KernelTestCase
{
    /**
     * @var TaskManagerInterface
     */
    private $taskManager;

    /**
     * @var TaskRunnerInterface
     */
    private $taskRunner;

    protected function setUp()
    {
        self::bootKernel();
        $this->taskManager = static::$kernel->getContainer()->get('background_task.manager');
        $this->taskRunner = static::$kernel->getContainer()->get('background_task.runner');
    }

    public function testGetProviders()
    {
        $tagList = ['foo', 'bar'];

        $providers = $this->taskRunner->getProviders();

        $this->assertGreaterThanOrEqual(2, count($providers));

        foreach ($tagList as $tag) {
            $this->assertArrayHasKey($tag, $providers);
        }
    }

    public function testRunTask()
    {
        $result = $this->taskRunner->runTask(new Task(0, 'foo.pow', [2, 2]));
        $this->assertEquals(4, $result, 'pow(2,2) must be 4');

        $result = $this->taskRunner->runTask(new Task(0, 'foo.abs', [-2]));
        $this->assertEquals(2, $result, 'abs(-2) must be 2');

        $result = $this->taskRunner->runTask(new Task(0, 'foo.round', [2.2]));
        $this->assertEquals(2, $result, 'round(2.2) must be 2');

        $result = $this->taskRunner->runTask(new Task(0, 'bar.trim', [' a ']));
        $this->assertEquals('a', $result, "trim(' a ') must be a");

        $this->expectException(\Exception::class);
        $this->taskRunner->runTask(new Task(0, 'bar.exception'));
    }

    public function testRunTaskNotFound()
    {
        $this->expectException(\Exception::class);
        $this->taskRunner->runTask(new Task(0, 'bar.notFound'));
    }
}
