<?php

namespace TextMedia\BackgroundTaskBundle\Tests\Test;

use PHPUnit\Framework\TestCase;

class CodeStyleTest extends TestCase
{
    const PHPCS_BIN = './vendor/bin/phpcs';

    public function testCodeStyle()
    {
        $this->assertFileExists(self::PHPCS_BIN);

        $result = passthru(self::PHPCS_BIN, $return_var);
        $this->assertEquals(0, $return_var, $result);
    }
}
