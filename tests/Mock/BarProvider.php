<?php

namespace TextMedia\BackgroundTaskBundle\Tests\Mock;

use TextMedia\BackgroundTaskBundle\ProviderInterface;

class BarProvider implements ProviderInterface
{
    /**
     * @inheritdoc
     */
    public function getBackgroundCallbacks(): array
    {
        return [
            'bar.trim' => 'trim',
            'bar.exception' => function () {
                throw new \Exception('Exception', 100);
            }
        ];
    }
}
