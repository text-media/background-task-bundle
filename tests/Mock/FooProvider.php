<?php

namespace TextMedia\BackgroundTaskBundle\Tests\Mock;

use TextMedia\BackgroundTaskBundle\ProviderInterface;

class FooProvider implements ProviderInterface
{
    /**
     * @inheritdoc
     */
    public function getBackgroundCallbacks(): array
    {
        return [
            'foo.pow' => 'pow',
            'foo.abs' => function ($value) {
                return abs($value);
            },
            'foo.round' => [$this, 'round']
        ];
    }

    public function round($value)
    {
        return round($value);
    }
}
