<?php

namespace TextMedia\BackgroundTaskBundle;

use TextMedia\BackgroundTaskBundle\Entity\Task;

interface TaskManagerInterface
{
    /**
     * Добавление задачи в очередь.
     *
     * @param int       $objectId   ID объекта привязки
     * @param string    $name       Имя фоновой задачи
     * @param array     $params     Дополнительные параметры.
     * @param \DateTime $deferredTo Дата отложенного старта.
     *
     * @return Task
     */
    public function addTask(int $objectId, string $name, array $params = [], \DateTime $deferredTo = null): Task;
}
