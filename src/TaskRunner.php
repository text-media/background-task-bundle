<?php

namespace TextMedia\BackgroundTaskBundle;

use TextMedia\BackgroundTaskBundle\Entity\Task;

class TaskRunner implements TaskRunnerInterface
{
    /**
     * @var ProviderInterface[]
     */
    protected $providers = [];

    /**
     * @var callable[]
     */
    protected $callbacks = [];

    /**
     * @inheritdoc
     */
    public function addProvider(ProviderInterface $provider, ?string $alias = null)
    {
        if ($alias === null) {
            $alias = get_class($provider);
        }

        $this->providers[$alias] = $provider;
        $this->callbacks = array_merge($this->callbacks, $provider->getBackgroundCallbacks());
    }

    /**
     * @inheritdoc
     */
    public function getProviders(): array
    {
        return $this->providers;
    }

    /**
     * @inheritdoc
     */
    public function runTask(Task $task)
    {
        return call_user_func_array($this->callbacks[$task->getName()] ?? function () use ($task) {
            throw new \Exception("Task '{$task->getName()}' is not registered.");
        }, $task->getParams());
    }
}
