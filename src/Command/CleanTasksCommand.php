<?php

namespace TextMedia\BackgroundTaskBundle\Command;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use TextMedia\BackgroundTaskBundle\Entity\Task;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CleanTasksCommand extends Command
{
    /**
     * @var EntityRepository
     */
    private $repository;

    public function __construct(ManagerRegistry $doctrine)
    {
        parent::__construct();

        $this->repository = $doctrine->getRepository(Task::class, 'background_task');
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('background:tasks:clean')
            ->setDescription('Запустить очистку завершенных фоновых задач двухнедельной давности');
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $queryBuilder = $this->repository->createQueryBuilder('t')
            ->delete()
            ->where('t.state != :state')
            ->andWhere('t.startedAt < :startedAt')
            ->setParameters([
                'state'     => Task::STATE_STARTED,
                'startedAt' => new \DateTime('-2 weeks'),
            ]);

        $queryBuilder->getQuery()->execute();

        return Command::SUCCESS;
    }
}
