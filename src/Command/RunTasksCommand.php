<?php

namespace TextMedia\BackgroundTaskBundle\Command;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Logger\ConsoleLogger;
use TextMedia\BackgroundTaskBundle\DeferredException;
use TextMedia\BackgroundTaskBundle\Entity\Task;
use TextMedia\BackgroundTaskBundle\TaskRunnerInterface;
use Doctrine\Persistence\ObjectManager;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RunTasksCommand extends Command
{
    /**
     * @var TaskRunnerInterface
     */
    private $taskRunner;

    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var EntityRepository
     */
    private $repository;

    /**
     * @var boolean
     */
    private $runned = true;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(TaskRunnerInterface $taskRunner, ManagerRegistry $doctrine)
    {
        parent::__construct();

        $this->taskRunner    = $taskRunner;
        $this->doctrine      = $doctrine;
        $this->entityManager = $doctrine->getManager('background_task');
        $this->repository    = $this->entityManager->getRepository(Task::class);
    }

    /**
     * @inheritdoc
     */
    protected function configure()
    {
        $this
            ->setName('background:tasks:run')
            ->setDescription('Запустить выполнение очереди фоновых задач')
            ->addOption('daemon', 'd', InputOption::VALUE_OPTIONAL, 'Режим демона', 0)
            ->addOption('timeout', 't', InputOption::VALUE_OPTIONAL, 'Время ожидания между запусками обработки задач в режиме демона (сек.)', 5)
            ->addOption('falltime', 'f', InputOption::VALUE_OPTIONAL, 'Время до следующей попытки выполнить задачу (сек.)', 10)
        ;
    }

    /**
     * @inheritdoc
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->logger = new ConsoleLogger($output);
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $daemonMode = (bool) $input->getOption('daemon');

        return $daemonMode ? $this->executeDaemon($input) : $this->executeTasks($input);
    }

    /**
     * Запустить режим демона
     *
     * @param InputInterface $input
     *
     * @return int
     */
    protected function executeDaemon(InputInterface $input)
    {
        pcntl_async_signals(true);

        $timeout = (int) $input->getOption('timeout');
        $this->logger->info('Демон запущен');

        pcntl_signal(SIGTERM, function () {
            $this->logger->info('Получен сигнал SIGTERM');
            $this->stop();
        });

        while ($this->runned) {
            $this->pingConnections();
            $this->executeTasks($input);

            if ($this->runned) {
                sleep($timeout);
            }
        }

        return Command::SUCCESS;
    }

    /**
     * Возвращает время до следующей попытки выполнить задачу (сек.)
     * В дефолтном поведении - это значение переданной опции при запуске задачи/демона,
     * но в подклассах можно переопределить метод и реализовать произвольное расписание.
     *
     * @param Task $task
     * @param int  $default
     *
     * @return int|null
     */
    protected function getFalltime(Task $task, int $default)
    {
        return $default;
    }

    /**
     * Запустить режим демона
     *
     * @param InputInterface $input
     *
     * @return int
     */
    protected function executeTasks(InputInterface $input)
    {
        if (!$this->runned) {
            return Command::SUCCESS;
        }

        $defaultFalltime = (int) $input->getOption('falltime');

        $queryBuilder = $this->repository->createQueryBuilder('t')
            ->where('t.state = :state')
            ->andWhere('t.deferredTo <= :now')
            ->setParameters([
                'state' => Task::STATE_STARTED,
                'now'   => new \DateTime(),
            ]);

        try {
            /** @var Task[] $tasks */
            $tasks = $queryBuilder->getQuery()->getResult();
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage());
            return Command::FAILURE;
        }

        $this->logger->debug('Задач на выполнение: ' . count($tasks));

        foreach ($tasks as $task) {
            if (!$this->runned) {
                return Command::SUCCESS;
            }

            try {
                $task->start();
                $this->entityManager->flush($task);

                try {
                    $this->taskRunner->runTask($task);
                    $this->logger->info('Задача выполнена', ['task' => $task->getId(), 'name' => $task->getName(), 'object' => $task->getObjectId()]);
                    $task->success();
                } catch (DeferredException $e) {
                    $task->setFailInfo($e);

                    $falltime = $this->getFalltime($task, $defaultFalltime);
                    if ($falltime === null) {
                        $task->fail();
                        $this->logger->error($e->getMessage(), ['task' => $task->getId(), 'name' => $task->getName(), 'object' => $task->getObjectId()]);
                    } else {
                        $task->defer(new \DateTime("+{$falltime} sec"));
                        $this->logger->warning($e->getMessage(), ['task' => $task->getId(), 'name' => $task->getName(), 'object' => $task->getObjectId()]);
                    }
                } catch (\Exception $e) {
                    $task->setFailInfo($e);
                    $task->fail();
                    $this->logger->error($e->getMessage(), ['task' => $task->getId(), 'name' => $task->getName(), 'object' => $task->getObjectId()]);
                }

                $this->entityManager->flush($task);
                $this->clearEntityManagers();
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage());
            }
        }

        return Command::SUCCESS;
    }

    /**
     * Очищает все менеджеры кроме своего
     */
    protected function clearEntityManagers()
    {
        foreach ($this->doctrine->getManagers() as $manager) {
            /** @var ObjectManager $manager */
            if ($manager !== $this->entityManager) {
                $manager->clear();
            }
        }
    }

    /**
     * Проверяет все коннекты и переподключается при необходимости
     */
    protected function pingConnections()
    {
        foreach ($this->doctrine->getConnections() as $name => $connection) {
            /** @var Connection $connection */
            if (!$connection->ping()) {
                $connection->close();
                $this->logger->error("{$name} DB server has gone away. Waiting for reconnect.");
                while (!$connection->connect()) {
                    $this->logger->info("{$name} DB not ping.");
                    if (!$this->runned) {
                        return;
                    }
                    sleep(1);
                }
                $this->logger->info("{$name} DB connected.");
                sleep(1);
            }
        }
    }

    /**
     * Останавливает демона
     */
    protected function stop()
    {
        $this->runned = false;
    }
}
