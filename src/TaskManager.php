<?php

namespace TextMedia\BackgroundTaskBundle;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Persistence\ObjectManager;
use TextMedia\BackgroundTaskBundle\Entity\Task;

class TaskManager implements TaskManagerInterface
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * TaskManager constructor.
     *
     * @param Registry $doctrine
     */
    public function __construct(Registry $doctrine)
    {
        $this->objectManager = $doctrine->getManager('background_task');
        $this->repository    = $this->objectManager->getRepository(Task::class);
    }

    /**
     * @inheritdoc
     */
    public function addTask(int $objectId, string $name, array $params = [], \DateTime $deferredTo = null): Task
    {
        $task = new Task($objectId, $name, $params, $deferredTo);
        $this->objectManager->persist($task);
        $this->objectManager->flush();

        return $task;
    }
}
