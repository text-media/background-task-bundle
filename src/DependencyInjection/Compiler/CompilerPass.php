<?php

namespace TextMedia\BackgroundTaskBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class CompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (false === $container->hasDefinition('background_task.runner')) {
            return;
        }

        $definition = $container->getDefinition('background_task.runner');

        foreach ($container->findTaggedServiceIds('background_task.provider') as $id => $providers) {
            foreach ($providers as $provider) {
                $definition->addMethodCall('addProvider', [
                    new Reference($id),
                    $provider['alias'] ?? null,
                ]);
            }
        }
    }
}
