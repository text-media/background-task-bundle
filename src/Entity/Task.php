<?php

namespace TextMedia\BackgroundTaskBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Блокировка
 *
 * @ORM\Table(name="background_tasks", indexes={
 *     @ORM\Index(name="IDX_object_id", columns={"object_id"}),
 *     @ORM\Index(name="IDX_background_tasks_state", columns={"state"}),
 *     @ORM\Index(name="IDX_background_tasks_deferred_to", columns={"deferred_to"})
 * })
 * @ORM\Entity
 */
class Task
{
    /**
     * Задача запланирована
     */
    const STATE_STARTED = 1;

    /**
     * Задача выполнена успешно
     */
    const STATE_SUCCESS = 2;

    /**
     * Задача остановлена с ошибкой
     */
    const STATE_FAIL = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * ID объекта привязки
     *
     * @var int
     *
     * @ORM\Column(name="object_id", type="integer")
     */
    private $objectId;

    /**
     * Имя задачи
     *
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * Дополнительные параметры к задаче
     *
     * @var array
     *
     * @ORM\Column(name="params", type="json_array")
     */
    private $params;

    /**
     * Состояние задачи
     *
     * @var int
     *
     * @ORM\Column(name="state", type="integer")
     */
    private $state;

    /**
     * Счетчик запусков задачи
     *
     * @var int
     *
     * @ORM\Column(name="counter", type="integer")
     */
    private $counter;

    /**
     * Дата создания задачи
     *
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * Дата последнего запуска задачи
     *
     * @var \DateTime|null
     *
     * @ORM\Column(name="started_at", type="datetime", nullable=true)
     */
    private $startedAt;

    /**
     * Дата отложенного запуска
     *
     * @var \DateTime
     *
     * @ORM\Column(name="deferred_to", type="datetime")
     */
    private $deferredTo;

    /**
     * Дата последней неудачи запуска задачи
     *
     * @var \DateTime|null
     *
     * @ORM\Column(name="failed_at", type="datetime", nullable=true)
     */
    private $failedAt;

    /**
     * Код последней неудачи запуска задачи
     *
     * @var integer|null
     *
     * @ORM\Column(name="fail_code", type="integer", nullable=true)
     */
    private $failCode;

    /**
     * Текст последней неудачи запуска задачи
     *
     * @var string|null
     *
     * @ORM\Column(name="fail_message", type="string", nullable=true)
     */
    private $failMessage;

    /**
     * Lock constructor.
     *
     * @param int       $objectId   ID объекта привязки
     * @param string    $name       Имя фоновой задачи
     * @param array     $params     Параметры
     * @param \DateTime $deferredTo Дата отложенного старта.
     */
    public function __construct(int $objectId, string $name, array $params = [], \DateTime $deferredTo = null)
    {
        $this->objectId   = $objectId;
        $this->name       = $name;
        $this->counter    = 0;
        $this->params     = $params;
        $this->createdAt  = new \DateTime();
        $this->deferredTo = $deferredTo ?: new \DateTime();
        $this->state      = self::STATE_STARTED;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getObjectId(): int
    {
        return $this->objectId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @return int
     */
    public function getState(): int
    {
        return $this->state;
    }

    /**
     * @return int
     */
    public function getCounter(): int
    {
        return $this->counter;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getDeferredTo(): \DateTime
    {
        return $this->deferredTo;
    }

    /**
     * @return \DateTime
     */
    public function getStartedAt()
    {
        return $this->startedAt;
    }

    /**
     * @return \DateTime
     */
    public function getFailedAt(): \DateTime
    {
        return $this->failedAt;
    }

    /**
     * @return int|null
     */
    public function getFailCode()
    {
        return $this->failCode;
    }

    /**
     * @return string
     */
    public function getFailMessage()
    {
        return $this->failMessage;
    }

    /**
     * Зафиксировать старт задачи
     */
    public function start()
    {
        $this->startedAt = new \DateTime();
        $this->counter++;
    }

    public function setFailInfo(\Exception $exception)
    {
        $this->failedAt    = new \DateTime();
        $this->failCode    = $exception->getCode();
        $this->failMessage = $exception->getMessage();
    }

    /**
     * Отложить запуск
     *
     * @param \DateTime $deferredTo Дата отложенного старта.
     */
    public function defer(\DateTime $deferredTo = null)
    {
        $this->deferredTo = $deferredTo ?: new \DateTime();
    }

    /**
     * Зафиксировать неудачное завершение
     *
     * @param string $failMessage Сообщение ошибки.
     */
    public function fail()
    {
        $this->state = self::STATE_FAIL;
    }

    /**
     * Зафиксировать удачное завершение
     */
    public function success()
    {
        $this->state = self::STATE_SUCCESS;
    }
}
