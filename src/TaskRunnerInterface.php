<?php

namespace TextMedia\BackgroundTaskBundle;

use TextMedia\BackgroundTaskBundle\Entity\Task;

interface TaskRunnerInterface
{
    /**
     * Регистрация обработчика задач.
     *
     * @param ProviderInterface $provider
     * @param string            $alias
     */
    public function addProvider(ProviderInterface $provider, string $alias);

    /**
     * Получение зарегистрированных обработчиков задач.
     *
     * @return ProviderInterface[]
     */
    public function getProviders(): array;

    /**
     * Запуск задачи
     *
     * @param Task $task
     *
     * @return mixed
     * @throws \Exception
     */
    public function runTask(Task $task);
}
