<?php

namespace TextMedia\BackgroundTaskBundle;

use TextMedia\BackgroundTaskBundle\DependencyInjection\Compiler\CompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Бандл управления фоновыми задачами
 */
class BackgroundTaskBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new CompilerPass());
    }
}
