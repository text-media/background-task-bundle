<?php

namespace TextMedia\BackgroundTaskBundle;

interface ProviderInterface
{
    /**
     * Получить список обработчиков по именам задач.
     *
     * @return callable[]
     */
    public function getBackgroundCallbacks(): array;
}
